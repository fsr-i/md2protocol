/**
 * class Renderer
 *
 * Generates HTML from parsed token stream. Each instance has independent
 * copy of rules. Those can be rewritten with ease. Also, you can add new
 * rules if you create plugin and adds new token types.
 **/
'use strict';


const lescape = require('escape-latex');
let escapeLatex = input => lescape(input, {
	preserveFormatting: true,
	escapeMapFn: function(defaultEscapes, formattingEscapes) {
		delete formattingEscapes[" "];
		delete formattingEscapes["\n"];
		defaultEscapes['~'] = '$\\sim$';
		return Object.assign({}, defaultEscapes, formattingEscapes);
	},
});

function slugifyWithUTF8 (text) {
  // remove HTML tags and trim spaces
  //let newText = stripTags(text.toString().trim())
  let newText = text.toString().trim()
  // replace space between words with dashes
  newText = newText.replace(/\s+/g, '-')
  // slugify string to make it valid as an attribute
  newText = newText.replace(/([!"#$%&'()*+,./:;<=>?@[\\\]^`{|}~])/g, '')
  // lets avoid some utf8 stuff
  newText = newText
  	.replace('ä', 'ae').replace('Ä', 'AE')
  	.replace('ö', 'oe').replace('Ö', 'OE')
  	.replace('ü', 'ue').replace('Ü', 'UE')
  	.replace('ß', 'ss').replace('ß', 'SS')
  return newText
}

const createHeaderId = (headerContent, headerIds = null, prefix = '') => {
  // to escape characters not allow in css and humanize
  const slug = slugifyWithUTF8(headerContent)
  let id
  const idBase = prefix + slug.toLowerCase()
  id = idBase
  if (headerIds !== null) {
    // ... making sure the id is unique
    let i = 1
    while (headerIds.indexOf(id) >= 0) {
      id = idBase + '-' + i
      i++
    }
    headerIds.push(id)
  }
  return id
}


//var assign          = require('./common/utils').assign;
var assign          = require('./node_modules/markdown-it/lib/common/utils').assign;

// Merge objects
//
//function assign(obj /*from1, from2, from3, ...*/) {
/*  var sources = Array.prototype.slice.call(arguments, 1);

	sources.forEach(function (source) {
		if (!source) { return; }

		if (typeof source !== 'object') {
			throw new TypeError(source + 'must be object');
		}

		Object.keys(source).forEach(function (key) {
			obj[key] = source[key];
		});
	});

	return obj;
}*/


//var unescapeAll     = require('./common/utils').unescapeAll;
var unescapeAll     = require('./node_modules/markdown-it/lib/common/utils').unescapeAll;
/*
var UNESCAPE_MD_RE  = /\\([!"#$%&'()*+,\-.\/:;<=>?@[\\\]^_`{|}~])/g;
var ENTITY_RE       = /&([a-z#][a-z0-9]{1,31});/gi;
var UNESCAPE_ALL_RE = new RegExp(UNESCAPE_MD_RE.source + '|' + ENTITY_RE.source, 'gi');

var DIGITAL_ENTITY_TEST_RE = /^#((?:x[a-f0-9]{1,8}|[0-9]{1,8}))/i;

var entities = require('./entities');

function replaceEntityPattern(match, name) {
	var code = 0;

	if (has(entities, name)) {
		return entities[name];
	}
*/
//  if (name.charCodeAt(0) === 0x23/* # */ && DIGITAL_ENTITY_TEST_RE.test(name)) {
/*    code = name[1].toLowerCase() === 'x' ?
			parseInt(name.slice(2), 16) : parseInt(name.slice(1), 10);

		if (isValidEntityCode(code)) {
			return fromCodePoint(code);
		}
	}

	return match;
}

function unescapeMd(str) {
	if (str.indexOf('\\') < 0) { return str; }
	return str.replace(UNESCAPE_MD_RE, '$1');
}

function unescapeAll(str) {
	if (str.indexOf('\\') < 0 && str.indexOf('&') < 0) { return str; }

	return str.replace(UNESCAPE_ALL_RE, function (match, escaped, entity) {
		if (escaped) { return escaped; }
		return replaceEntityPattern(match, entity);
	});
}*/


//var escapeHtml      = require('./common/utils').escapeHtml;
var escapeHtml      = require('./node_modules/markdown-it/lib/common/utils').escapeHtml;
/*
var HTML_ESCAPE_TEST_RE = /[&<>"]/;
var HTML_ESCAPE_REPLACE_RE = /[&<>"]/g;
var HTML_REPLACEMENTS = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;'
};

function replaceUnsafeChar(ch) {
	return HTML_REPLACEMENTS[ch];
}

function escapeHtml(str) {
	if (HTML_ESCAPE_TEST_RE.test(str)) {
		return str.replace(HTML_ESCAPE_REPLACE_RE, replaceUnsafeChar);
	}
	return str;
}*/


////////////////////////////////////////////////////////////////////////////////

var important_rules = {};


important_rules.footnote_ref = function (tokens, idx, options, env, slf) {
	//console.log(env.footnotes);
	if (tokens[idx].meta.label) {
		return '{\\footnotemark['+tokens[idx].meta.label+']}';
	} else {
		return '\\footnote{'+env.footnotes.list[tokens[idx].meta.id].content+'}';
	}
}

important_rules.footnote_reference_open = function (tokens, idx, options, env, slf) {
	//console.log('footnote_reference_open', tokens[idx], tokens[idx+1], tokens[idx+2], tokens[idx+3], '\n');
	if (tokens[idx+1].type == 'paragraph_open') {
		tokens[idx+1].hidden = true;
	}
	if (tokens[idx+3].type == 'paragraph_close') {
		tokens[idx+3].hidden = true;
	}
	return '\\footnotetext['+tokens[idx].meta.label+']{';
}

important_rules.footnote_reference_close = function (tokens, idx, options, env, slf) {
	//console.log('footnote_reference_close', tokens[idx], tokens[idx+1], tokens[idx+2], tokens[idx+3], '\n');
	return '}\n';
}


important_rules.footnote_block_open = function (tokens, idx, options, env, slf) {
	//console.log('footnote_block_open', tokens[idx], tokens[idx+1], tokens[idx+2], tokens[idx+3], '\n');
	return '';
}

important_rules.footnote_block_close = function (tokens, idx, options, env, slf) {
	//console.log('footnote_block_close');
	//console.log(env.footnotes, tokens[idx], tokens[idx+1], '\n');
	return '';
}

important_rules.footnote_open = function (tokens, idx, options, env, slf) {
	if (tokens[idx+1].type == 'paragraph_open') {
		tokens[idx+1].hidden = true;
	}
	if (tokens[idx+2].type == 'inline') {
		tokens[idx+2].hidden = true;
		tokens[idx+2].content = true;
		tokens[idx+2].children = [];
	}
	//console.log('footnote_open', tokens[idx], tokens[idx+1], tokens[idx+2], '\n');
	return '';
}

important_rules.footnote_close = function (tokens, idx, options, env, slf) {
	//console.log('footnote_close', tokens[idx], tokens[idx+1], '\n');
	return '';
}

important_rules.footnote_anchor = function (tokens, idx, options, env, slf) {
	//console.log('footnote_anchor', tokens[idx], tokens[idx+1], '\n');
	return '';
}




var default_rules = {};


default_rules.code_inline = function (tokens, idx, options, env, slf) {
	var token = tokens[idx];

	return	'\\texttt{' +
			escapeLatex(tokens[idx].content) +
			'}';
};


default_rules.code_block = function (tokens, idx, options, env, slf) {
	var token = tokens[idx];

	return  '\\begin{lstlisting}\n' +
			//escapeLatex(tokens[idx].content) +
			tokens[idx].content +
			'\\end{lstlisting}\n';
};


default_rules.fence = function (tokens, idx, options, env, slf) {
	var token = tokens[idx],
		info = token.info ? unescapeAll(token.info).trim() : '',
		langName = '',
		highlighted, i, tmpAttrs, tmpToken;

	let lstlisting_langs = 'ABAP, ACSL, Ada, Algol, Ant, Assembler, Awk, bash, Basic, C#, C++, C, Caml, Clean, Cobol, Comal, csh, Delphi, Eiffel, Elan, erlang, Euphoria, Fortran, GCL, Gnuplot, Haskell, HTML, IDL, inform, Java, JVMIS, ksh, Lisp, Logo, Lua, make, Mathematica, Matlab, Mercury, MetaPost, Miranda, Mizar, ML, Modelica, Modula-2, MuPAD, NASTRAN, Oberon-2, Objective C, OCL, Octave, Oz, Pascal, Perl, PHP, PL/I, Plasm, POV, Prolog, Promela, Python, R, Reduce, Rexx, RSL, Ruby, S, SAS, Scilab, sh, SHELXL, Simula, SQL, tcl, TeX, VBScript, Verilog, VHDL, VRML, XML, XSLT'.split(', ');

	if (info) {
		let lang = info.match(/^\s*([^\s]+)(\s|$)/g)[0];
		let i = lstlisting_langs.map(e=>e.toLowerCase()).indexOf(lang.toLowerCase());
		if (i >= 0) {
			langName = lstlisting_langs[i];
		}
	}

	return  '\\begin{lstlisting}'+
			(langName && '[language='+langName+']')+'\n' +
			//escapeLatex(tokens[idx].content) +
			tokens[idx].content +
			'\\end{lstlisting}\n';
/*
	if (highlighted.indexOf('<pre') === 0) {
		return highlighted + '\n';
	}

	// If language exists, inject class gently, without modifying original token.
	// May be, one day we will add .clone() for token and simplify this part, but
	// now we prefer to keep things local.
	if (info) {
		i        = token.attrIndex('class');
		tmpAttrs = token.attrs ? token.attrs.slice() : [];

		if (i < 0) {
			tmpAttrs.push([ 'class', options.langPrefix + langName ]);
		} else {
			tmpAttrs[i][1] += ' ' + options.langPrefix + langName;
		}

		// Fake token just to render attributes
		tmpToken = {
			attrs: tmpAttrs
		};

		return  '<pre><code' + slf.renderAttrs(tmpToken) + '>'
				+ highlighted
				+ '</code></pre>\n';
	}


	return  '<pre><code' + slf.renderAttrs(token) + '>'
			+ highlighted
			+ '</code></pre>\n';*/
};

/*
default_rules.image = function (tokens, idx, options, env, slf) {
	var token = tokens[idx];

	// "alt" attr MUST be set, even if empty. Because it's mandatory and
	// should be placed on proper position for tests.
	//
	// Replace content with actual value

	token.attrs[token.attrIndex('alt')][1] =
		slf.renderInlineAsText(token.children, options, env);

	return slf.renderToken(tokens, idx, options);
};
*/

default_rules.hardbreak = function (tokens, idx, options, env) {
	return '\\\\\n';
};
default_rules.softbreak = function (tokens, idx, options, env) {
	return options.breaks ? '\\\\\n' : '\n';
};


default_rules.text = function (tokens, idx, options, env) {
	return escapeLatex(tokens[idx].content);
};

/*
default_rules.html_block = function (tokens, idx, options, env) {
	return tokens[idx].content;
};
default_rules.html_inline = function (tokens, idx, options, env) {
	return tokens[idx].content;
};*/


/**
 * new Renderer()
 *
 * Creates new [[Renderer]] instance and fill [[Renderer#rules]] with defaults.
 **/
function Renderer() {

	/**
	 * Renderer#rules -> Object
	 *
	 * Contains render rules for tokens. Can be updated and extended.
	 *
	 * ##### Example
	 *
	 * ```javascript
	 * var md = require('markdown-it')();
	 *
	 * md.renderer.rules.strong_open  = function () { return '<b>'; };
	 * md.renderer.rules.strong_close = function () { return '</b>'; };
	 *
	 * var result = md.renderInline(...);
	 * ```
	 *
	 * Each rule is called as independent static function with fixed signature:
	 *
	 * ```javascript
	 * function my_token_render(tokens, idx, options, env, renderer) {
	 *   // ...
	 *   return renderedHTML;
	 * }
	 * ```
	 *
	 * See [source code](https://github.com/markdown-it/markdown-it/blob/master/lib/renderer.js)
	 * for more details and examples.
	 **/
	this.rules = assign({}, default_rules);
	this.important_rules = important_rules;
}


/**
 * Renderer.renderAttrs(token) -> String
 *
 * Render token attributes to string.
 **/
Renderer.prototype.renderAttrs = function renderAttrs(token) {
	var i, l, result;

	if (!token.attrs) { return ''; }

	result = '';

	for (i = 0, l = token.attrs.length; i < l; i++) {
		result += ' ' + escapeLatex(token.attrs[i][0]) + '="' + escapeLatex(token.attrs[i][1]) + '"';
	}

	return result;
};



Renderer.prototype._url_to_file_id = function _url_to_file_id(url) {
	url = new URL(url);
	// split off the extension
	let ext_match = url.pathname.match(/^(.+)(\.[^.]{1-8})?$/);
	// now, append query stuff before the extentension
	let name = url.origin +
		(ext_match[1] || '') + 
		url.search + 
		(ext_match[2] || '');
	name = name.replace(/[^_\-+0-9A-Za-z]+/g, '-');
	if (this.DOWNLOAD_DIR !== undefined) {
		name = this.DOWNLOAD_DIR + '/' + name;
	}
	return name;
}


Renderer.prototype._find_or_get_file = function _find_or_get_file(url) {
	let file_id = this._url_to_file_id(url);
	// get file, save to file_id
	if (this.downloader) {
		// downloader might give us a different file name where the extension should definitely match the mime type
		return this.downloader.smart_download(url, file_id);
	} else {
		console.warn('\n\t####  Please download the following file to include it on LaTeX compilation:  ####\nURL:         '+url+'\nDestination: '+file_id+'\n\n');
	}
	return file_id;
}


/**
 * Renderer.renderToken(tokens, idx, options) -> String
 * - tokens (Array): list of tokens
 * - idx (Numbed): token index to render
 * - options (Object): params of parser instance
 *
 * Default token renderer. Can be overriden by custom function
 * in [[Renderer#rules]].
 **/
Renderer.prototype.renderToken = function renderToken(tokens, idx, options, env, slf) {
	var nextToken,
			result = '',
			needLf = false,
			token = tokens[idx];

	// Tight list paragraphs
	if (token.hidden && token.tag != 'hr') {
		return '';
	}

	// Insert a newline between hidden paragraph and subsequent opening
	// block-level tag.
	//
	// For example, here we should insert a newline before blockquote:
	//  - a
	//    >
	//
	if (token.block && token.nesting !== -1 && idx && tokens[idx - 1].hidden) {
		result += '\n';
	}


////////// change from here /////////////

//return JSON.stringify(token)+'\n\n' || '';

switch (token.tag) {
	case 'p':
		return '\n';
	case 'h1':
		let top_regex = new RegExp('^\\s*(zu |RE)?TOP\\s*([0-9]+):\s*(.+)$');
		if (token.nesting !== -1) {
			nextToken = tokens[idx+1];
			// if the very first heading is something boring like "FSR-Sitzung", silently omit it - there's a \maketitle in latex for that
			if (!env.firstH1) {
				env.firstH1 = nextToken.content;
				if (nextToken.content.match(/^(FSR|Fachschaftsrats?)?-?(Groß(e )?|Haupt|Neben)?sitzung$/i)) {
					token.hidden = true;
					nextToken.hidden = true;
					nextToken.children = [];
					//nextToken.content = "";
					tokens[idx+2].hidden = true;
					return '';
				}
			}
			let match = nextToken.content.match(top_regex);
			if (match) {
				let name = match[3].trim();
				let retop = match[1] ? true : false;
				// nested "text" token inside "inline" - we just want to modify the outer .content (that isn't used for rendering but contains the same information initially)
				//nextToken.content = name;

				// write into toplist (meta info)
				if (!retop && this._md !== undefined) {
					if (this._md.meta === undefined) this._md.meta = {};
					if (this._md.meta.toplist === undefined) this._md.meta.toplist = [];
					this._md.meta.toplist.push(name);
				}

				// cut out "TOP X: " part from first child token (not re-using from variable name here, because there might be other stuff in tokens and this might only hold the fist part, not the full headline)
				nextToken.children[0].content = nextToken.children[0].content.match(top_regex)[3].trimLeft();

				if (this._slugs === undefined) {
					this._slugs = [];
				}

				let slug = createHeaderId(
					this.renderInlineAsText(nextToken.children, options),
					!retop ? this._slugs : null
				);

				if (!retop) {
					return '\n'+'\t'.repeat(token.level)+'\\top{'+slug+'}[';
				} else {
					/*if (!this._md.meta.toplist || this._md.meta.toplist.indexOf(name) < 0) {
						console.log('### problematic RETOP:', name, match);
						return '\n'+'\t'.repeat(token.level)+'\\section{zu \\ref{top:'+slug+'}: ';
					} else {*/
						console.log('### RETOP:', name, match);
						nextToken.hidden = true;
						nextToken.children = [];
						return '\n'+'\t'.repeat(token.level)+'\\retop{'+slug+'}';
					//}
				}
			}
		} else {
			let match = tokens[idx-1].content.match(top_regex);
			if (match) {
				let retop = match[1];
				if (retop) {
					/*if (!this._md.meta.toplist || this._md.meta.toplist.indexOf(match[3].trim()) < 0) {
						return '}\n'+'\t'.repeat(token.level)+'\\label{pretop:'+this._slugs[this._slugs.length-1]+'}\n';
					} else {*/
						return '\n';
					//}
				} else {
					return ']\n';
				}
			}
		}
	case 'h2':
	case 'h3':
	case 'h4':
		if (token.nesting !== -1) {
			let subs = parseInt(token.tag.slice(-1)-1);
			if (this._slugs === undefined) {
				this._slugs = [];
			}
			let slug = createHeaderId(
				this.renderInlineAsText(tokens[idx+1].children, options),
				this._slugs,
				(subs < 3 ? 's'.repeat(subs) : '')+'sec:'
			);

			if (subs < 3) {
				return '\n'+'\t'.repeat(token.level)+'\\'+'sub'.repeat(subs)+'section{';
			} else {
				return '\n\t\\'+(subs > 3 ? 'sub' : '')+'paragraph{';
			}
		} else {
			return '}\n'+('\t'.repeat(token.level))+'\\label{'+this._slugs[this._slugs.length-1]+'}\n';
		}
	case 'strong':
		return token.nesting !== -1 ? '\\textbf{' : '}';
	case 'em':
		return token.nesting !== -1 ? '\\textit{' : '}';
	case 'ins':
		return token.nesting !== -1 ? '\\hul{' : '}';
	case 's':
		return token.nesting !== -1 ? '\\hsout{' : '}';
	case 'mark':
		return token.nesting !== -1 ? '\\hl{' : '}';
	case 'a':
		var attrs = {};
		if (token.attrs) {
			for (let i in token.attrs) {
				attrs[token.attrs[i][0]] = escapeLatex(token.attrs[i][1]);
			}
		};
		return token.nesting !== -1 ? '\\href'+(attrs.href ? '{'+attrs.href+'}' : '')+'{' : '}';
	case 'blockquote':
		if (this._md !== undefined) {
			let metaq = this._parseMetaQuote(tokens, idx, options);
			if (metaq) {
				this._md.meta = this._md.meta ? Object.assign(this._md.meta, metaq) : metaq;
				return '';
			}
		}

		// detect "appear"/"disappear" messages
		let time_re = '([0-9]?[0-9][:.][0-9][0-9]( Uhr)?)?';
		//let appear_regex = new RegExp('^\\s*(.+) (erschein(t|en) zur|verl(ässt|assen) die) Sitzung\\.?\\s*$', 'i');
		let appear_regex = new RegExp('^[\\s\\*]*'+time_re+'[:\\*\\s]*(.+) (erschein(t|en) zur|verl(ässt|assen) die) Sitzung\\.?\\s*'+time_re+'$', 'i');
		if (tokens.length > idx+6 &&
			tokens[idx+1].type == 'blockquote_open' && 
			tokens[idx+2].type == 'paragraph_open' && 
			tokens[idx+3].type == 'inline' && 
			tokens[idx+5].type == 'blockquote_close' && 
			tokens[idx+6].type == 'blockquote_close') {
			let match = this.renderInlineAsText(tokens[idx+3].children, options).match(appear_regex);
			if (match) {
				let people = match[3];
				let out = '\n\\'
				if (match[4].startsWith('verl')) {
					out += 'dis';
				} else if (!match[4].startsWith('ersch')) {
					out = null;
				}

				if (out) {
					out += 'appear';
					if ((match[5] || match[6]).endsWith('en')) {
						out += 'p';
					}
					out += '{'+people+'}';
					if (match[1]) {
						out += '\t% '+match[1]
					}
					out += '\n'

					for (let i = idx; i <= idx+6; i++) {
						tokens[i].content = '';
						tokens[i].hidden = true;
						tokens[i].type = 'text';
					}
					return out;
				}
			} else {
				console.log(' no match.', tokens[idx+3].content, appear_regex);
			}
		}
		return token.nesting !== -1 ? '\n'+'\t'.repeat(token.level)+'\\begin{interrupt}' : '\t'.repeat(token.level)+'\\end{interrupt}\n\n';
		//return token.nesting !== -1 ? '\n'+'\t'.repeat(token.level)+'\\blockquote{' : '\t'.repeat(token.level)+'}\n\n';
	case 'ul':
		return token.nesting !== -1 ? (token.level > 1 ? '\\\n' : '\n')+'\t'.repeat(token.level)+'\\begin{itemize}\n' : '\t'.repeat(token.level)+'\\end{itemize}\n';
	case 'ol':
		return token.nesting !== -1 ? (token.level > 1 ? '\\\n' : '\n')+'\t'.repeat(token.level)+'\\begin{enumerate}\n' : '\t'.repeat(token.level)+'\\end{enumerate}\n';
	case 'li':
		return token.nesting !== -1 ? '\t'.repeat(token.level)+'\\item ' : '\n';
	case 'img':
		return '\\includegraphics[width=\\linewidth,height=.5\\linewidth,keepaspectratio]{'+this._find_or_get_file(token.attrs.find(e => e[0])[1])+'}\n';
	case 'hr':
		if (tokens.length > idx+4 &&
			(tokens[idx+1].tag == 'h3' || tokens[idx+1].tag == 'h4' || tokens[idx+1].type == 'paragraph_open') &&
			tokens[idx+2].type == 'inline' &&
			(tokens[idx+3].tag == 'h3' || tokens[idx+3].tag == 'h4' || tokens[idx+3].type == 'paragraph_close') &&
			tokens[idx+4].type == 'hr') {
			let vote_regex = /^\s*(Vote:|Abstimmung:)?\s*(.+)\s*\(([0-9]*)[^0-9A-Za-z]([0-9]*)[^0-9A-Za-z]([0-9]*)\)/i;
			let opinion_regex = /^\s*(Public opinion:|Meinungsbild:)?\s*(.+)\s*\(([0-9]*)[^0-9A-Za-z]([0-9]*)\)/i;
			let decision_regex = /^\s*(Decision:|Beschluss:)\s*(.+)/i;

			let clear = function() {
				for (let i = idx; i < idx+4; i++) {
					tokens[i].content = '';
					tokens[i].hidden = true;
					tokens[i].type = 'text';
				}
				tokens[idx+4].hidden = true;
				//console.log('cleared tokens:',tokens.slice(idx,idx+4));
			}

			let match = escapeLatex(tokens[idx+2].content).match(vote_regex);
			if (match) {
				clear();
				return '\\vote{'+match[2].trim()+'}{'+match[3]+'}{'+match[4]+'}{'+match[5]+'}\n';
			}
			match = tokens[idx+2].content.match(opinion_regex);
			if (match) {
				clear();
				return '\\opinion{'+match[2].trim()+'}{'+match[3]+'}{'+match[4]+'}\n';
			}
			match = tokens[idx+2].content.match(decision_regex);
			if (match) {
				clear();
				return '\\decision{'+match[2].trim()+'}\n';
			}
		}
		if (token.hidden) {
			return '';
		}
		return token.nesting !== -1 ? '\n'+'\t'.repeat(token.level)+'\\vspace{1mm}\\hrulefill\\vspace{3mm}\n\n' : '';
	case 'table':
		if (token.nesting !== -1) {
			// get number of columns
			token.maxCols = 0;
			let i = 1;
			while (idx+i < tokens.length) {
				if (tokens[idx+i].type == 'tr_close') {
					break;
				}
				if (tokens[idx+i].type == 'th_open') {
					token.maxCols++;
				}
				i++;
			}

			// save number of columns to all tokens of this table
			i = 1;
			let row = 0;
			let col = 0;
			while (tokens[idx+i].type !== 'table_close') {
				if (tokens[idx+i].type.match(/^t(able|r|h|d|head|body)_/)) {
					tokens[idx+i].maxCols = token.maxCols;
					if (tokens[idx+i].type.match(/^t(h|d)_/)) {
						tokens[idx+i].row = row;
						tokens[idx+i].column = col;
						if (tokens[idx+i].type.endsWith('_open')) {
							col++;
						}
					}
					if (tokens[idx+i].type.startsWith('tr_close')) {
						tokens[idx+i].row = row;
						row++;
						col = 0;
					}
				}
				i++;
			}
		}

		let columns = '{'+('l'.repeat(token.maxCols).split('').join(' | '))+'}';
		return token.nesting !== -1 ? '\n'+'\t'.repeat(token.level)+'\\begin{tabular}'+columns+'\n' : '\t'.repeat(token.level)+'\\end{tabular}\n';
	case 'tr':
		if (token.nesting !== -1) {
			return '\t'.repeat(token.level-1);
		} else {
			if (tokens[idx+1].type == 'tbody_close') {
				return '\n';
			} else {
				return '\\\\\n';
			}
		}
	case 'th':
		if (token.nesting !== -1) {
			return '\\textbf{';
		} else {
			return token.column < token.maxCols ? '}\t&\t' : '}';
		}
	case 'td':
		if (token.nesting !== -1) {
			return '';
		} else {
			return token.column < token.maxCols ? '\t&\t' : '';
		}
	case 'thead':
		if (token.nesting === -1) {
			return '\\hline';
		}
	case 'tbody':
		return '';
	default:
		return JSON.stringify(token)+'\n\n';
		//return token.nesting !== -1 ? '<'+token.tag+this.renderAttrs(token)+'>' : '</'+token.tag+'>';
}


	// Add token name, e.g. `<img`
//  result += (token.nesting === -1 ? '</' : '<') + token.tag;

	// Encode attributes, e.g. `<img src="foo"`
//  result += this.renderAttrs(token);

	// Add a slash for self-closing tags, e.g. `<img src="foo" /`
/*  if (token.nesting === 0 && options.xhtmlOut) {
		result += ' /';
	}*/

	// Check if we need to add a newline after this tag
/*  if (token.block) {
		needLf = true;

		if (token.nesting === 1) {
			if (idx + 1 < tokens.length) {
				nextToken = tokens[idx + 1];

				if (nextToken.type === 'inline' || nextToken.hidden) {
					// Block-level tag containing an inline tag.
					//
					needLf = false;

				} else if (nextToken.nesting === -1 && nextToken.tag === token.tag) {
					// Opening tag + closing tag of the same type. E.g. `<li></li>`.
					//
					needLf = false;
				}
			}
		}
	}*/

//  result += needLf ? '>\n' : '>';


////////// to here /////////////


	return result;
};


/**
 * Renderer.renderInline(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * The same as [[Renderer.render]], but for single token of `inline` type.
 **/
Renderer.prototype.renderInline = function (tokens, options, env) {
	var type,
			result = '',
			rules = Object.assign(this.rules, this.important_rules);

	for (var i = 0, len = tokens.length; i < len; i++) {
		type = tokens[i].type;

		if (typeof rules[type] !== 'undefined') {
			result += rules[type](tokens, i, options, env, this);
		} else {
			result += this.renderToken(tokens, i, options);
		}
	}

	return result;
};


/** internal
 * Renderer.renderInlineAsText(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Special kludge for image `alt` attributes to conform CommonMark spec.
 * Don't try to use it! Spec requires to show `alt` content with stripped markup,
 * instead of simple escaping.
 **/
Renderer.prototype.renderInlineAsText = function (tokens, options, env) {
	var result = '';

	for (var i = 0, len = tokens.length; i < len; i++) {
		if (tokens[i].type === 'text') {
			result += tokens[i].content;
			//result += escapeLatex(tokens[i].content);
		} else if (tokens[i].type === 'image') {
			result += this.renderInlineAsText(tokens[i].children, options, env);
		}
	}

	return result;
};


/**
 * Renderer.render(tokens, options, env) -> String
 * - tokens (Array): list on block tokens to renter
 * - options (Object): params of parser instance
 * - env (Object): additional data from parsed input (references, for example)
 *
 * Takes token stream and generates HTML. Probably, you will never need to call
 * this method directly.
 **/
Renderer.prototype.render = function (tokens, options, env) {
	var i, len, type,
			result = '',
			rules = Object.assign(this.rules, this.important_rules);

	for (i = 0; i < tokens.length; i++) {
		type = tokens[i].type;

		if (type === 'inline') {
			result += this.renderInline(tokens[i].children, options, env);
		} else if (typeof rules[type] !== 'undefined') {
			result += rules[tokens[i].type](tokens, i, options, env, this);
		} else {
			result += this.renderToken(tokens, i, options, env);
		}
	}

	if (!env.no_document) {
		result = this.startLatex(tokens, options, env)
				+ result
				+ this.endLatex(tokens, options, env);
	}

	return result;
};


Renderer.prototype.startLatex = function (tokens, options, env) {
	let m = (function(n) {
		let o = '\\'+n+'{';
		if (this._md && this._md.meta && this._md.meta[n]) {
			o += this._md.meta[n];
		}
		o += '}';
		return o;
	}).bind(this);

	let out = [
		'\\documentclass[a4paper]{fsr-protocol2}',
		'',
		(
			this._md.meta &&
			this._md.meta.title && 
			this._md.meta.title.toLowerCase().indexOf('nebensitzung') > -1 ?
				'\\title{Nebensitzung}' :
				''
		),
		m('author'),
		m('chairperson'),
		m('date'),
		m('voted'),
		m('coopted'),
		m('honorary'),
		m('guests'),
		m('excused'),
		m('missing'),
		m('coronacommissioner'),
		m('Sstart')+' % start of the session',
		m('Send')+' % end of the session',
		'',
		this._md.meta && this._md.meta.yesquorate ? '\\yesquorate' : '%\\yesquorate',
		this._md.meta && this._md.meta.noquorate ? '\\noquorate' : '%\\noquorate',
		'',
		'\\begin{document}',
		'',
		'\\maketitle',
		''
	].join('\n');

	return out+'\n';
};

let i = 0;
Renderer.prototype.endLatex = function (tokens, options, env) {
	let out = [
		'\\end{document}'
	].join('\n');

	return '\n'+out+'\n';
};


Renderer.prototype._parseMetaQuote = function (tokens, idx, options) {
	// only allow first quote to be meta
	if (tokens.map(e=>e.type).indexOf('blockquote_open') !== idx) {
		return;
	}

	let parse = function(tokens, idx, options) {
		let meta = {};
		let quote_level = 1;

		let line = '';
		let line_regex = new RegExp('^\\s*([^:]+):\\s*(.*)\\s*$|Der FSR ist ((nicht )?beschlussfähig)');
		let translations = {
			author: ['protokollführung','autor'],
			chairperson: ['sitzungsleitung','leitung'],
			date: ['datum'],
			voted: ['gewählt','gewählte'],
			coopted: ['kooptiert','kooptierte'],
			guests: ['gäste'],
			excused: ['entschuldigt','entschuldigte'],
			missing: ['fehlend','fehlende', 'unentschuldigt', 'unentschuldigte'],
			honorary: ['ehrenmitglied','ehrenmitglieder', 'ehren-mitglied', 'ehren-mitglieder'],
			coronacommissioner: ['coronabeauftragter','coronabeauftragte', 'corona-beauftragter', 'corona-beauftragte'],
			Sstart: ['sitzungsstart','start'],
			Send: ['sitzungsende','ende'],

			yesquorate: ['beschlussfähig'],
			noquorate: ['nicht beschlussfähig']
		};

		let translate = function(name) {
			name = name.toLowerCase();
			for (let i in translations) {
				if (name == i.toLowerCase() || translations[i].indexOf(name) >= 0) {
					return i;
				}
			}
		}
		let finalize_line = function() {
			if (line.trim().length > 0) {
				let match = line.match(line_regex);
				if (match === null || match === undefined) {
					//console.log(match,':',line);
				} else if (match) {
					if (match[3]) {
						match[1] = match[3];
						match[2] = true;
					}
					let tr = translate(match[1]);
					if (tr) {
						meta[tr] = match[2];
					} else {
						console.error('  could not find translation for key:', match[1], '(value: '+match[2]+')');
					}
				}
				line = '';
			}
		}

		for (let i = idx+1; i < tokens.length; i++) {
			let token = tokens[i];
			//console.log(token);
			switch (token.type) {
				case 'blockquote_open':
					quote_level++;
					break;
				case 'blockquote_close':
					quote_level--;
					break;
				case 'inline':
					meta = Object.assign(meta, parse(token.children, 0, options));
					break;
				case 'text':
					line += token.content;
					break;
				case 'softbreak':
					finalize_line();
					break;
				default:
					break;
			}

			// hide token - somehow, setting 'hidden' here doesn't suffice
			token.type = 'text';
			token.tag = '';
			token.content = '';
			token.hidden = true;

			if (quote_level == 0) {
				break;
			}
		}

		finalize_line();

		return meta;
	}

	return parse(tokens, idx, options);
};


module.exports = Renderer;
