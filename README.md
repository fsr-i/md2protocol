# TOP 1: Markdown und *md2latex*

Markdown ist eine sehr einfache und **menschenlesbare Markupsprache**.
Selbst, wenn *kein* geeigneter Interpreter zur grafischen Darstellung zur Hand ist, lässt es sich beinahe so mühelos lesen, als wenn es gar nicht selbst eine Art Quelltext wäre.

Leider genügt dieses Format nicht dem Anspruch einer wissenschaftlichen Drucksache.
Aus diesem Grund wurde *md2latex* geschaffen -- ein Compiler zu LaTeX.

## Markdown-it rulezz!

Es gibt zwar einige Compiler für Markdown, jedoch besitzt dieses keinen Standard und existiert so in vielen verschiedenen Varianten.
*md2latex* möchte die Features von [HedgeDoc](https://demo.hedgedoc.org/features) abbilden, die sehr viel auf angenehme Weise integrieren.

HedgeDoc nutzt intern *markdown-it*, *md2latex* hat diese Lösung minimal nachgebaut und den HTML-Renderer auf LaTeX umgefriemelt.
Wem das zu unsauber ist, der darf gerne selber eine bessere Lösung beisteuern.


# TOP 2: How to use

```
node compile.js [input.md] [output.tex] [meta.json]
```

If you want to test it, you can run the `compile.js` without any arguments. It will convert the example document, which you are reading right now, to LaTeX and print it to stdout.

When converting your own documents, specify the path to your input markdown as the first argument.
If you don't want to scrape the result from stdout, you can specify another path as the second argument and `compile.js` will write the LaTeX to there.

Markdown documents can also contain meta data (as YAML) at the beginning like this:

```
---
title: The Two-s Day
date: 22.02.2022
author: Me
---

# Headline

This is the actual start of the markdown document itself...
```

`compile.js` will parse this and use it for the `\maketitle` stuff.
It does this very straightforward though, by putting every key--value pair as `\key{value}` into the LaTeX document---you have been warned.
If you want to use this information somehow, because it was such an easy thing to implement, you can specify a path as the third argument where this meta information will be written to in JSON format.



# TOP 3: Most important features

Test **bold** *it* [l**in**k](test.bla) drone.io.

> test1
> test2
> test3

----

- A
- B
  - 1
  - 2
- `Code`!

```Python
more
code()
```

>> Max Scholz erscheint zur Sitzung.

> > Max Scholz verlässt die Sitzung.

> > Max Scholz, Niklas Merkelt erscheinen zur Sitzung

>> 14:32 Uhr Max Scholz, Niklas Merkelt verlassen die Sitzung.

## Bilder

Die Einbindung von Bildern ist -- mehr noch als das ganze Projekt -- als äußerst experimentell einzustufen.
Bilder sind in (HedgeDoc-flavoured) Markdown nicht im eigentlichen Sinne enthalten, sondern nur als Verweise auf eine Ressource, welcher von etwaigen darstellenden Programmen zu interpretieren ist.
Während es sich hier um URLs handelt, kennt LaTeX nur Verweise auf das lokale Dateisystem und fügt die Ressourcen zur Compile-Zeit in das Produkt (in den meisten Fällen die PDF) ein.

Es wurden Anstrengungen unternommen, etwaige entfernte Ressourcen automatisch und asynchron herunterzuladen und lokal bereitzustellen.
Das tatsächliche Verhalten könnte jedoch stark von der gestellten Erwartung abweichen, hierauf gibt der Autor keine Gewähr.
