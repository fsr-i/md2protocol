#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const latex_renderer = require('./latex-renderer');
const downloader = (()=>{
	try {
		return require('./downloader');
	} catch (e) {
		console.warn("Not utilising automatic media downloader, as it failed to initialise (this is NOT AN ERROR):\n\n", e);
		console.log();
		return null;
	}
})()


const md = require('markdown-it')({
  html:         false,        // Enable HTML tags in source
  xhtmlOut:     false,        // Use '/' to close single tags (<br />).
                              // This is only for full CommonMark compatibility.
  breaks:       true,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks. Can be
                              // useful for external highlighters.
  linkify:      true,         // Autoconvert URL-like text to links

  // Enable some language-neutral replacement + quotes beautification
  typographer:  false,
  quotes: '„“‚‘'
});
md.inline.ruler.__rules__[md.inline.ruler.__find__('link')].fn = require('./rules-inline-link.js');
md.renderer = new latex_renderer();
md.renderer.downloader = downloader ? new downloader() : null;
md.renderer.DOWNLOAD_DIR = 'downloaded_images';

const meta = require('./meta.js');
md.use(meta);

md.use(require('./semantically-true-footnotes.js'));
//md.use(require('markdown-it-footnote'));
md.use(require('markdown-it-ins'));
md.use(require('markdown-it-sup'));
md.use(require('markdown-it-sup'));
md.use(require('markdown-it-mark'));
md.use(require('markdown-it-imsize'));

const Plugin = require('markdown-it-regexp');

// TOC
const tocPlugin = new Plugin(
  /^\s*\[TOC\]\s*$/i,
  (match, utils) => '\\tableofcontents'
);
md.use(tocPlugin);

// math
const mathCenteredPlugin = new Plugin(
  /(^|[^\\])\$\$(.*[^\\])\$\$/i,
  (match, utils) => match[0]
);
md.use(mathCenteredPlugin);

const mathInlinePlugin = new Plugin(
  /(^|[^\\])\$(.*[^\\])\$/i,
  (match, utils) => match[0]
);
md.use(mathInlinePlugin);

// <br>
const brPlugin = new Plugin(
  /<br\s*\/?>/i,
  (match, utils) => '\\\\\n'
);
md.use(brPlugin);

// ignore <details>
const ignoreDetailsPlugin = new Plugin(
  /^(<details>(<summary>(.*)<\/summary>)|<\/details>(<br\s?\/?>)?)?$/i,
  (match, utils) => match[3] ? md.renderInline(match[3], Object.assign({}, {no_document: true})) : ''
);
md.use(ignoreDetailsPlugin);


async function main() {
	let args = process.argv.slice(2)
	if (args.length > 0) {
		fs.readFile(args[0], {encoding: 'utf-8'}, (err, data) => {
			if (err) console.error('failed to read file '+args[0], err);
			
			out = md.render(data);
			
			function finish() {
				if (args.length > 1) {
					fs.writeFile(args[1], out, err => {
						if (err) throw err;
					});
					if (args.length > 2) {
						fs.writeFile(args[2], JSON.stringify(md.meta), err => {
							if (err) throw err;
						})
					}
				} else {
					console.log(out);
				}
			}
			if (md.renderer.downloader) {
				md.renderer.downloader.replace_renamed_files(out).then(o => {
					out = o;
					finish();
				});
			} else {
				finish();
			}
		});
	} else {
		let result = md.render('---\ntitle: FSR Nebensitzung 2022-11-23\ntags: sitzung, nebensitzung, protokoll\n---\n\n> **Datum: 23.11.2022**\n> Sitzungsleitung: Adam Bartel\n> Protokollführung: Eva Char\n> Sitzungsstart: 14:XX\n> Sitzungsende: 15:XX\n> \n> ***Anwesende***\n> *Gewählte:* Adam Bartel\n> *Kooptierte:* Eva Char\n> *Entschuldigte:* \n> *Gäste:* Edith Fink\n> *Fehlend:* \n> **Der FSR ist nicht beschlussfähig.**\n\n\n# TOP 1: Markdown und *md2latex*\n\nMarkdown ist eine sehr einfache und **menschenlesbare Markupsprache**.\nSelbst, wenn *kein* geeigneter Interpreter zur grafischen Darstellung zur Hand ist, lässt es sich beinahe so mühelos lesen, als wenn es gar nicht selbst eine Art Quelltext wäre.\n\nLeider genügt dieses Format nicht dem Anspruch einer wissenschaftlichen Drucksache.\nAus diesem Grund wurde *md2latex* geschaffen -- ein Compiler zu LaTeX.\n\n## Markdown-it rulezz!\n\nEs gibt zwar einige Compiler für Markdown, jedoch besitzt dieses keinen Standard und existiert so in vielen verschiedenen Varianten.\n*md2latex* möchte die Features von [HedgeDoc](https://demo.hedgedoc.org/features) abbilden, die sehr viel auf angenehme Weise integrieren.\n\nHedgeDoc nutzt intern *markdown-it*, *md2latex* hat diese Lösung minimal nachgebaut und den HTML-Renderer auf LaTeX umgefriemelt.\nWem das zu unsauber ist, der darf gerne selber eine bessere Lösung beisteuern.\n\n\n# TOP 2: How to use\n\n```\nnode compile.js [input.md] [output.tex] [meta.json]\n```\n\nIf you want to test it, you can run the `compile.js` without any arguments. It will convert the example document, which you are reading right now, to LaTeX and print it to stdout.\n\nWhen converting your own documents, specify the path to your input markdown as the first argument.\nIf you don\'t want to scrape the result from stdout, you can specify another path as the second argument and `compile.js` will write the LaTeX to there.\n\nMarkdown documents can also contain meta data (as YAML) at the beginning like this:\n\n```\n---\ntitle: The Two-s Day\ndate: 22.02.2022\nauthor: Me\n---\n\n# Headline\n\nThis is the actual start of the markdown document itself...\n```\n\n`compile.js` will parse this and use it for the `\\maketitle` stuff.\nIt does this very straightforward though, by putting every key--value pair as `\\key{value}` into the LaTeX document---you have been warned.\nIf you want to use this information somehow, because it was such an easy thing to implement, you can specify a path as the third argument where this meta information will be written to in JSON format.\n\n\n\n# TOP 3: Most important features\n\nTest **bold** *it* [l**in**k](test.bla) drone.io.\n\n> test1\n> test2\n> test3\n\n----\n\n- A\n- B\n  - 1\n  - 2\n- `Code`!\n\n```Python\nmore\ncode()\n```\n\n>> Max Scholz erscheint zur Sitzung.\n\n> > Max Scholz verlässt die Sitzung.\n\n> > Max Scholz, Niklas Merkelt erscheinen zur Sitzung\n\n>> 14:32 Uhr Max Scholz, Niklas Merkelt verlassen die Sitzung.\n\n## Bilder\n\nDie Einbindung von Bildern ist -- mehr noch als das ganze Projekt -- als äußerst experimentell einzustufen.\nBilder sind in (HedgeDoc-flavoured) Markdown nicht im eigentlichen Sinne enthalten, sondern nur als Verweise auf eine Ressource, welcher von etwaigen darstellenden Programmen zu interpretieren ist.\nWährend es sich hier um URLs handelt, kennt LaTeX nur Verweise auf das lokale Dateisystem und fügt die Ressourcen zur Compile-Zeit in das Produkt (in den meisten Fällen die PDF) ein.\n\nEs wurden Anstrengungen unternommen, etwaige entfernte Ressourcen automatisch und asynchron herunterzuladen und lokal bereitzustellen.\nDas tatsächliche Verhalten könnte jedoch stark von der gestellten Erwartung abweichen, hierauf gibt der Autor keine Gewähr.\n');
		console.log(result);
	}
}

main();
